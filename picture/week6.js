/**
 * Created by kk on 2018-1-30.
 */
 var nf=document.querySelector("#NewFolder");//新建文件夹按钮
 var lu=document.querySelector(".left ul");//左边相册栏
 var right=document.querySelector(".right");//右边显示栏
 var delalls=document.querySelectorAll(".remove");//删除相册按钮
 var classNum=1;
//新建相册：
//    1.新建相册名
//    2.给相册添加删除按钮
//    3.点击相册显示对应照片
 nf.addEventListener("click",function(){

     let str=prompt("新建相册名称");
     if(str!=null && str.replace(/\s+/g, "").length != 0){
         var a=document.createElement("a");
         a.href="javascript:;";
         a.innerHTML=str;
         var li=document.createElement("li");
         li.classList.add("all"+classNum);
         li.appendChild(a);
         lu.appendChild(li);

         classNum++;
         adddel();
         selectbox();
     }
 })

 //    给相册添加删除按钮
 function adddel(){
     let liall=lu.querySelectorAll("li");//所有相册栏
     let len=liall.length;
     let img=document.createElement("img");
     img.src="img/delete.ico";
     img.className="remove"
     liall[len-1].appendChild(img);
     //点击相册删除按钮，删除对应相册
     img.addEventListener("click",function(event){
         let par=event.target.parentNode;
         let lis=right.querySelectorAll("."+par.className);//删除按钮对应的相册名
         let len=lis.length;
         let all=lu.querySelector('.selectAll');//“全部”栏
         //如果要删除的相册是正在显示的相册，删除该相册后默认显示“全部”相册栏
         if(event.target.previousSibling.classList.contains("fontChange")){
             all.childNodes[0].classList.add("fontChange");
             view();
         }
         //删除相册时，为该相册内的照片清除与该相册的class绑定
         for(let i=0;i<len;i++){
             lis[i].classList.remove(par.className);
         }

         lu.removeChild(par);
         alert("删除相册内的照片在全部相册内依然存在");

     })
 }

 //点击相册显示对应照片
 function selectbox(){
     let liina=lu.querySelectorAll("li a");
     let alen=liina.length;
     let current=lu.querySelector(".fontChange");//当前显示的相册
     for(let i=0;i<alen;i++){
         liina[i].addEventListener("click",function(){
             current.classList.remove("fontChange");
             this.classList.add("fontChange");
             current=this;
             view();
         })
     }
 }
 //右侧照片显示
 function view(){
     let classNameBox=lu.querySelector(".fontChange").parentNode.className;

     let liBox=right.querySelectorAll("li");//所有照片
     let len=liBox.length;
     console.log("1")

     for(let i=0;i<len;i++){
         if(classNameBox=='selectAll'){
             if(!liBox[i].classList.contains("show")){
                 liBox[i].classList.add("show")
             }
             if(liBox[i].classList.contains("hidden")){
                 liBox[i].classList.remove("hidden")
             }
         }
         else {
             if(liBox[i].classList.contains(classNameBox)){
                 if(!liBox[i].classList.contains("show")){
                     liBox[i].classList.add("show")
                 }
                 if(liBox[i].classList.contains("hidden")){
                     liBox[i].classList.remove("hidden")
                 }

             }
             else {
                 if(!liBox[i].classList.contains("hiddden")){
                     liBox[i].classList.add("hidden")
                 }
                 if(liBox[i].classList.contains("show")){
                     liBox[i].classList.remove("show")
                 }
             }
         }

     }
     Paging(1);
 }

 //选择照片并显示
 var file=document.querySelector("#file");
 file.addEventListener("change",function(){
     var iLen=this.files.length;

     for(let i=0;i<iLen;i++){
         //实例化一个FileReader
         var reader = new FileReader();
         let filename=this.files[i].name;

         //加载成功时获取图片名和图片内容
         reader.onload=function (){
                 let imgMsg={
                     name:filename,
                     base64:this.result//reader.readAsDataURL方法执行完后，base64数据储存在reader.result里
                 }
                 let liAll=document.createElement("li");
                 let parentCurrent=lu.querySelector(".fontChange").parentNode;
                 let classNameBox=parentCurrent.className;
                 let ul=right.querySelector("ul");
                 liAll.classList.add(classNameBox);
                 liAll.classList.add("pos");
                 liAll.classList.add("show");
                 liAll.innerHTML='<div class="imgSize"><img src="'+imgMsg.base64+'"></div><img src="img/remove.png" class="del"><span>'+imgMsg.name+'</span>'
                 ul.appendChild(liAll);
                 Paging(1);
             var del=liAll.querySelector(".del");
             del.addEventListener("click",function(){
                 delPhoto(this);
                 Paging(1);
             })
             }
         //转换数据格式
         reader.readAsDataURL(this.files[i]);

     }


 })
 //删除照片
 function delPhoto(e){
             if(confirm("确认删除吗")){
                 e.parentNode.parentNode.removeChild(e.parentNode);
             }
             else{
                 return;
             }
         }

 //分页
 function  Paging(index){
     var RankInfo=right.querySelectorAll("ul li.show");
     var totalPage=RankInfo.length;
     var pageSize=10;
     var pageNumber=Math.ceil(totalPage/pageSize);
     var currentPage=index;
     var start_row=(currentPage-1)*pageSize;
     var end_row=currentPage*pageSize;
     end_row=(end_row>totalPage)?totalPage:end_row;
     for(var i=0;i<totalPage;i++){
         var irow=RankInfo[i];
         if(i>=start_row && i<end_row){
             irow.style.display="block";
         }
         else {
             irow.style.display="none";
         }
     }

     var pageHTML="";
     pageHTML += "<a class='p_first' href=\"javascript:Paging(1)\" title=\"首页\"><span class='button'>首页</span></a>";
     var up =parseInt(currentPage)-1;
     if(up<1){up =1;}
     pageHTML += "<a class='p_prev' href=\"javascript:Paging("+up+")\" title=\"上一页\"><span class='button'>上一页</span></a>";
     pageHTML+="<span>"+currentPage+"/"+pageNumber+"</span>";
     var next =parseInt(currentPage)+1;
     if(next >pageNumber){ next = pageNumber ;}
     pageHTML += "<a class='p_next js_page' href=\"javascript:Paging("+next+")\" title=\"下一页\"><span class='button'>下一页</span></a>";
     pageHTML += "<a  class='p_last js_page' href=\"javascript:Paging("+pageNumber+")\" title=\"尾页\"><span class='button'>尾页</span></a>";
     var page=document.querySelector(".page");
     if(totalPage == 0){
         page.innerHTML="";
     }else {
         page.innerHTML=pageHTML;
     }

 }
